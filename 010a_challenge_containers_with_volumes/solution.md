## Edit Code Running In Containers With Bind Mounts
```bash
//cd to empty directory
docker run -v $(pwd):/site bretfisher/jekyll new .
docker run -p 80:4000 -d -v $(pwd):/site bretfisher/jekyll-serve
```