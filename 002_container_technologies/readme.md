# Introduction to Container Technology

## The idea of Containers
* Comparing to VMs
* Install to Windows...

## Transitions of server technology
* In 90s: Mainframe to PC
* In 00s: Baremetal to Virtual
* In 10s: Datacenter to Cloud
* Now: Host to Container

## Available Container Runtimes
![https://miro.medium.com/max/1050/1*OnB-Ah-FehjzQfhu5-BHmQ.png](https://miro.medium.com/max/1050/1*OnB-Ah-FehjzQfhu5-BHmQ.png)

## Docker
* Docker went after a different target market, developers, 
* Take containers beyond the OS level ... to the application itself
* It started out being built on top of LXC, 
* Docker later moved beyond LXC containers to its own execution environment called libcontainer. 
    * Unlike LXC, which launches an operating system init for each container, 
    * Docker provides one OS environment, 
    * Supplied by the Docker Engine, and 
* Enables developers to easily run applications that reside in their own application environment 
* Which is specified by a docker image. 
* Just like with LXC, these images can be shared among developers, 
* With a **dockerfile** 

## VMs vs Docker Containers
* Docker containers are much more lightweight compared to VMs
* Faster to deploy, smaller images, easier to configure
* Are just processes
* Limited to what resource they can access
* Exit when its (root) process stops
* The host shares its kernel
* That’s why there are windows and linux containers…

