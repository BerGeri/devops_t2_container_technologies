## Get the context of Docker Container
![](img/cont_context.png)

## Docker CLI
Docker CLI
* Controls the Docker daemon, which runs the containers
* Some commands, which will be used:
* **docker version**
* **docker info**
* **docker container** `run`, `stop`, `kill`, `attach`, `exec`, `inspect`, `ls`, `port`, `start`
* **docker image** `build`, `ls`, `rm`, `pull`, `push`
* **docker network**
* Docker command line structure:
* New style: docker `<command> <subcommand> (options)`
* Old style: docker `<command>(options)`

## Docker on Windows
* Running docker on windows or mac will use a virtual environment to
run Linux containers
* Tips on Windows: works only on pro, folder have to be shared, Hyper-V creates a virtual pc, install VS code and docker extension,
GitHub desktop for easy cloning
* On windows Ctrl+C send the container to background but not stop!