# Handling data persistance

## Why not storing data in the container?

* We can, but its an antipattern.
* Containers designed to be immutable and ephemeral
* Use them like we can throw them away any time.
* If containers are like running programs in memory, storing data in them is like storing program data by memory dumps

## How persistence in Docker works?

* Remember union file system? Its an other layer over the topmost
writable layer.
* A host folder or a Volume (a special Docker object) is mounted at a
specific path in the container
* The mounted content shadows the content originally was at that path
* Changes aren't stored in the container, but in the mounted path/Volume

## Bind mounts
* Its just a mapping between a host and a container folder.
* C:/example/folder/ (on the host) -> /var/content (in the container)
* Managed by the user
* Mount a folder to multiple containers is allowed but make it carefully! Docker not managing anything!
* Can't be declared in the Dockerfile. Set with docker run only!

## Docker Volumes
* This is the Docker managed version of mounts
* Much more safe and convenient use:
* OS independent
* Automatically created on demand
* Can be encrypted or stored on a remote host by storage drivers
* May have a name for easier usage
* It has to be declared in the Dockerfile
* Managed by Docker
* settings available via the CLI
