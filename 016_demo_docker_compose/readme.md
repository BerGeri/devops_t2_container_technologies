## Get a Docker Compose Sample
You can get awesome samples for compose from [Awesome Compose git repo](https://github.com/docker/awesome-compose).
```bash
git clone https://github.com/docker/awesome-compose
cd nginx-golang
tree
.
├── README.md
├── backend
│   ├── Dockerfile
│   └── main.go
├── docker-compose.yml
└── frontend
    ├── Dockerfile
    └── nginx.conf
```

## Deploy locally
```bash
docker-compose up -d
```