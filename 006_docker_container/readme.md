## Conatiners: A portable package of an application
* Containers are the runtime instances of images
* An Image is like an application executable on a hard drive
* A Container is like a running instance of an application
* So container is usually only one process, and shuts down when the process exits
* But: container data will not removed automatically when using Docker run

## Visiting containers

* `-it` means interactive and tty
* `docker container run -it` - start new container interactively
* `docker container exec -it` - run additional command in existing container
* `docker container top` - process list in one container
* `docker container inspect` - details of one container config
* `docker container stats` - performance stats for all containers