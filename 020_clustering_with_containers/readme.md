## The promise of containers
* We don't have to install applications manually, just use prebuilt images
* Containers running on workstations, on premises just like on cloud platforms of even on small devices like a raspberry!
* Wide support!

## Scale up/down (vertical scaling):
* more RAM, CPU or faster network to existing nodes 

## Scale in/out (horizontal scaling):
* more nodes to the system
* Now imagine, we have:
    * Several apps
    * Each app may use 20-30 services
    * Each service have many replicas
    * …and have to deal with:
        * Scale in/out infrastructure following daily usage
        * Upgrade services regularly
        * Every transition of infrastructure must be seamless! Users should not notice anything!

## Our new challange:

**automate controlling of the container lifecycle**
* Develop the image
* Develop content (code, database etc.) and dockerize it
* Test the image (standalone and integration!)
* Publish it to a repository
* Using image…
    * Pulling from a repository on each node
    * Starting, re-deploy on fail
    * Scaling in/out

**this is what orchestration about**

## But do we really need orchestration?
* It depends on several views:
    * size of services
    * upgrade frequency
    * reliability constraints
    * need of other features of an orchestrator
* Shouldn’t use if not necessary!
* it adds extra abstraction layers
* special knowledge required to design these systems to work perfectly