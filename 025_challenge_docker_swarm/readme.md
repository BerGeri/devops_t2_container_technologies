## Your challenge if you choose to accept it
1. Initalize a docker swarm in docker playground
1. Deploy wordpress based on the supplied docker-compose.yml file
1. Scale db to 3 replica sets
1. You can deploy any other docker-compose based project on your swarm for instance from awesome-compose project.